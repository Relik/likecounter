const rpio = require('rpio');
const Oled = require('sh1106-js');
const font = require('oled-font-5x7');

class DisplayManager {
    constructor() {
        rpio.init({
            gpiomem: false,
            mapping: 'physical',
        });
        this.oled = new Oled({ rpio });
        oled.clearDisplay();
        oled.turnOnDisplay();
    }

    displayLikes(type, likes) {
        oled.clearDisplay();
        oled.writeString(10, 10, font, type, 'WHITE');
        oled.writeString(10, 60, font, likes, 'WHITE');
    }
}

exports.DisplayManager = DisplayManager;