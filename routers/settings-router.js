var router = require('express').Router();
var settingsController = require('../controllers/settings-controller');

router.post('/', settingsController.settings);

exports.settingsRouter = router;