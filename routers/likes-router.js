var router = require('express').Router();
var likesController = require('../controllers/likes-controller');

router.get('/', likesController.likes);
router.get('/show', likesController.show);
router.get('/counter', likesController.counter);
exports.likesRouter = router;