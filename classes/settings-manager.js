var fs = require('fs');
var path = require('path');

const SETTINGS_LOCATION = path.join(ROOT_DIRECTORY + '/settings.json');
const SITE_LOCATION = path.join(ROOT_DIRECTORY + '/sites' + '/likes.html')

class SettingsManager {

    constructor() {
        this.settings = {
            fb_url: "",
            insta_url: "",
            likes: 0,
            followers: 0
        }
        this.loadSettings();
    }

    loadSettings() {
        return new Promise((resolve, reject) => {
            fs.readFile(SETTINGS_LOCATION, 'utf8', (err, jsonString) => {
                if (err) {
                    console.log("File read failed:", err)
                    reject();
                }

                this.updateSettings(JSON.parse(jsonString));
                console.log('File data:', this.settings)
                resolve();
            });
        })
    }

    createUrlFromUsername(username) {
        return `https://www.instagram.com/${username}/?__a=1`
    }

    updateSettings(newSettings) {
        if (newSettings.insta_username) {
            this.settings.showing = "insta";
            this.settings.insta_url = this.createUrlFromUsername(newSettings.insta_username);
            this.settings.followers = 0;
        } else if (newSettings.facebook_url) {
            this.settings.showing = "fb";
            this.settings.fb_url = newSettings.facebook_url;
            this.settings.likes = 0;

        } else {
            this.settings.fb_url = newSettings.fb_url || this.settings.fb_url;
            this.settings.insta_url = newSettings.insta_url || this.settings.insta_url;
            this.settings.likes = newSettings.likes || this.settings.likes;
            this.settings.followers = newSettings.followers || this.settings.followers;
            this.settings.showing = newSettings.showing || this.settings.showing;
        }
        this.saveSettings();
    }
    async saveSettings() {
        console.log("Zapisuję:", this.settings)
        await fs.writeFile(SITE_LOCATION, site1 + this.settings.fb_url + site2, function (err) {
            if (err) {
                console.log(err);
            }
        });
        await fs.writeFile(SETTINGS_LOCATION, JSON.stringify(this.settings), function (err) {
            if (err) {
                console.log(err);
            }
        });
        return;
    }

}
exports.SettingsManager = SettingsManager;








var site1 = `<html>
<body>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-page" 
data-href="`
var site2 = `"
data-width="380" 
data-hide-cover="false"
data-show-facepile="false"></div>
</body>
</html>`;