const Browser = require('zombie');
const axios = require('axios');

//Browser.localhost('localhost', 3002);
//Browser.localhost('https://likes-counter.herokuapp.com');

class LikesCounter {
    constructor() {
    }

    findLikes() {
        const browser = new Browser();
        return new Promise((resolve, reject) => {

            browser.visit('https://likes-counter.herokuapp.com/likes', function () {
                try {
                    let likes = "" +
                        browser.querySelector('.fb_iframe_widget iframe')
                            .contentWindow.document.documentElement
                            .getElementsByClassName("_1drq")[0].innerHTML;

                    likes = likes.replace(/\D/g, '');
                    console.log("LIKE:", likes);
                    settingsManager.updateSettings({ likes: likes });
                    resolve(likes);
                }
                catch (err) {
                    console.log('Error in likes counter', err);
                    resolve(settingsManager.settings.likes);
                }
            });
        })
    }

    startCounting() {
        setTimeout(async () => {
            let likes = await this.findLikes();
            let type = (settingsManager.settings.showing === "fb") ? "Likes: " : "Followers: ";
            console.log(type + likes);
            // DisplayManager.displayText(type, likes)
            this.startCounting();
        }, 3000)
    }

    findFollowers() {
        return new Promise((resolve, reject) => {
            axios.get(settingsManager.settings.insta_url)
                .then(response => {
                    let followers = "" + response.data.graphql.user.edge_followed_by.count;
                    console.log("followerski", followers);
                    settingsManager.updateSettings({ followers });
                    resolve(followers);
                })
                .catch(err => {
                    console.log(err.stack);
                    resolve(settingsManager.settings.followers);
                });
        })
    }

    find() {
        if (settingsManager.settings.showing == "fb") {
            return this.findLikes();
        } else {
            return this.findFollowers();
        }
    }

    getLikes() {
        return settingsManager.settings.likes;
    }
}

exports.LikesCounter = LikesCounter;