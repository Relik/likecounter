const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');

ROOT_DIRECTORY = path.join(path.dirname(require.main.filename));
const LikesCounter = require('../classes/likes-counter').LikesCounter;
const SettingsManager = require('../classes/settings-manager').SettingsManager;
// const DisplayManager = require('../classes/display-manager').DisplayManager;
const likesRouter = require('./likes-router').likesRouter;
const rootRouter = require('./root-router').rootRouter;
const settingsRouter = require('./settings-router').settingsRouter;

likesCounter = new LikesCounter();
settingsManager = new SettingsManager();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

app.use('/likes', likesRouter);
app.use('/settings', settingsRouter);
app.use('/', rootRouter);

exports.app = app;



































